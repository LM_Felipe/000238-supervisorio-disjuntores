<?xml version="1.0" encoding="UTF-8"?>
<!-- |============================================================================= -->
<!-- |	Copyright (C) 1993-2004 Rockwell Software. All rights reserved.				-->
<!-- |																				-->
<!-- |  XML Transform for converting RSView 3.10 XML files to the RSView SE 3.20	-->
<!-- |	format.																		-->
<!-- |	Warning:																	-->
<!-- |		Changing this file will cause importing Graphics XML files to fail.		-->
<!-- |																				-->
<!-- |============================================================================= -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
<xsl:output method="xml" indent="yes" version="1.0" encoding="UTF-8" />

<xsl:variable name="projType">Gfx-SE1.xsd</xsl:variable>
<xsl:include href="Msi2Gfx1.xslt" /> <!-- include file that implements conversion -->

</xsl:stylesheet>
