<!-- |============================================================================= -->
<!-- |	Copyright (C) 1993-2004 Rockwell Software. All rights reserved.				-->
<!-- |																				-->
<!-- |  XML Transform for converting RSView 3.10 XML files to the RSView 3.20		-->
<!-- |	format.																		-->
<!-- |	Warning:																	-->
<!-- |		Changing this file will cause importing Graphics XML files to fail.		-->
<!-- |																				-->
<!-- |============================================================================= -->
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

<!-- main template -->
<xsl:template match="/">
	<gfx xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" >
		<xsl:attribute name="xsi:noNamespaceSchemaLocation">
			<xsl:value-of select="$projType"/>
		</xsl:attribute>
		<xsl:apply-templates select="gfx/*" />
	</gfx>
</xsl:template>

<!-- MSI object template -->
<xsl:template match="/gfx/msi">
	<xsl:if test="@id">
		<multistateIndicator>
			<!-- Add the common attributes for the Multistate indicator -->
			<xsl:attribute name="name"><xsl:value-of select="@id"/></xsl:attribute>
			<xsl:if test="@height">
				<xsl:attribute name="height"><xsl:value-of select="@height"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@width">
				<xsl:attribute name="width"><xsl:value-of select="@width"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@x">
				<xsl:attribute name="left"><xsl:value-of select="@x"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@y">
				<xsl:attribute name="top"><xsl:value-of select="@y"/></xsl:attribute>
			</xsl:if>

			<xsl:if test="@visible">
				<xsl:attribute name="visible"><xsl:value-of select="@visible"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@description">
				<xsl:attribute name="description"><xsl:value-of select="@description"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="$projType = 'Gfx-SE1.xsd'">
				<!-- only add for SE -->
				<xsl:if test="@tooltip">
					<xsl:attribute name="toolTipText"><xsl:value-of select="@tooltip"/></xsl:attribute>
				</xsl:if>
			</xsl:if>

			<!-- Add the general visual attributes -->
			<xsl:if test="@backstyle">
				<xsl:attribute name="backStyle"><xsl:value-of select="@backstyle"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@borderstyle">
				<xsl:attribute name="borderStyle">
					<xsl:choose>
						<xsl:when test="@borderstyle = 'raisedinset'">raisedInset</xsl:when>
						<xsl:otherwise><xsl:value-of select="@borderstyle"/></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@borderusesbackcolor">
			<xsl:attribute name="borderUsesBackColor"><xsl:value-of select="@borderusesbackcolor"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@borderwidth">
				<xsl:attribute name="borderWidth"><xsl:value-of select="@borderwidth"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@shape">
				<xsl:attribute name="shape"><xsl:value-of select="@shape"/></xsl:attribute>
			</xsl:if>

			<xsl:if test="@numstates">
				<xsl:attribute name="setLastStateId"><xsl:value-of select="@numstates"/></xsl:attribute>
			</xsl:if>
			<xsl:attribute name="currentStateId">0</xsl:attribute>
			<xsl:if test="@triggertype">
				<xsl:attribute name="triggerType">
					<xsl:choose>
						<xsl:when test="@triggertype = lsb">LSB</xsl:when>
						<xsl:otherwise><xsl:value-of select="@triggertype"/></xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>

			<xsl:call-template name="states"/> <!-- add all states -->
			<xsl:call-template name="animations"/> <!-- add all animations -->
			<xsl:call-template name="connections"/> <!-- add all connections -->
		</multistateIndicator>
	</xsl:if>
</xsl:template>

<!-- State elements templates -->
<xsl:template name="states">
	<xsl:if test="msi-states/msi-state">
		<!-- if we have one or more states then add them -->
		<states>
			<xsl:for-each select="msi-states/msi-state">
				<xsl:call-template name="state"/>
			</xsl:for-each>
		</states>
	</xsl:if>
</xsl:template>

<xsl:template name="state">
	<xsl:if test="@stateid">
		<!-- must have a state id to add the state -->
		<state>
			<xsl:attribute name="stateId">
				<xsl:choose>
					<xsl:when test="@stateid = 'error'">Error</xsl:when>
					<xsl:otherwise><xsl:value-of select="@stateid"/></xsl:otherwise>
				</xsl:choose>
			</xsl:attribute>

			<xsl:if test="@value">
				<xsl:attribute name="value"><xsl:value-of select="@value"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@blink">
				<xsl:attribute name="blink"><xsl:value-of select="@blink"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@backcolor">
				<xsl:attribute name="backColor"><xsl:value-of select="@backcolor"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@bordercolor">
				<xsl:attribute name="borderColor"><xsl:value-of select="@bordercolor"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@patterncolor">
				<xsl:attribute name="patternColor"><xsl:value-of select="@patterncolor"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@patternstyle">
				<xsl:attribute name="patternStyle">
					<xsl:call-template name="patternAttribute">
						<xsl:with-param name="pattern"><xsl:value-of select="@patternStyle"/></xsl:with-param>
					</xsl:call-template>
				</xsl:attribute>
			</xsl:if>

			<!-- add the caption and image elements if necessary -->
			<xsl:if test="caption">
				<xsl:call-template name="caption" />
			</xsl:if>
			<xsl:if test="image">
				<xsl:call-template name="image" />
			</xsl:if>
		</state>
	</xsl:if>
</xsl:template>

<!-- Caption element template -->
<xsl:template name="caption">
	<xsl:element name="caption">
		<xsl:if test="caption/@text">
			<xsl:attribute name="caption">
				<xsl:call-template name="convertNewline">
					<xsl:with-param name="in"><xsl:value-of select="caption/@text"/></xsl:with-param>
				</xsl:call-template>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="caption/@alignment">
			<xsl:attribute name="alignment">
				<xsl:call-template name="alignmentAttribute">
					<xsl:with-param name="align"><xsl:value-of select="caption/@alignment"/></xsl:with-param>
				</xsl:call-template>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="caption/@color">
			<xsl:attribute name="color"><xsl:value-of select="caption/@color"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="caption/@backcolor">
			<xsl:attribute name="backColor"><xsl:value-of select="caption/@backcolor"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="caption/@backstyle">
			<xsl:attribute name="backStyle"><xsl:value-of select="caption/@backstyle"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="caption/@blink">
			<xsl:attribute name="blink"><xsl:value-of select="caption/@blink"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="caption/@wordwrap">
			<xsl:attribute name="wordWrap"><xsl:value-of select="caption/@wordwrap"/></xsl:attribute>
		</xsl:if>

		<!-- if there is a font we must check the font defs elements from the old format -->
		<xsl:if test="caption/@font">
			<xsl:call-template name="fontAttributes">
				<xsl:with-param name="font" select="substring-after(caption/@font,'#')"/>
			</xsl:call-template>
		</xsl:if>
	</xsl:element>
</xsl:template>

<xsl:template name="convertNewline">
	<xsl:param name="in"/>
	
	<xsl:variable name="tmp1"><xsl:value-of select="substring-before($in, '\')"/></xsl:variable>
	<xsl:variable name="tmp2"><xsl:value-of select="substring-after($in, '\')"/></xsl:variable>
	<xsl:variable name="tmp3"><xsl:value-of select="substring-before($tmp2, 'n')"/></xsl:variable>
	<xsl:variable name="tmp4"><xsl:value-of select="substring-after($tmp2, 'n')"/></xsl:variable>
	
	<xsl:choose>
		<xsl:when test="not(contains($in, '\n'))">
			<xsl:value-of select="$in"/>
		</xsl:when>

		<xsl:otherwise>
			<xsl:choose>
				<xsl:when test="$tmp3 = ''">
					<xsl:value-of select="$tmp1"/><xsl:text>&#x0A;</xsl:text>
				</xsl:when>

				<xsl:when test="$tmp3 = '\'">
					<xsl:value-of select="$tmp1"/><xsl:text>\n</xsl:text>
				</xsl:when>

				<xsl:otherwise>
					<xsl:value-of select="$tmp1"/><xsl:text>\</xsl:text><xsl:value-of select="$tmp3"/><xsl:text>n</xsl:text>
				</xsl:otherwise>
			</xsl:choose>

			<xsl:if test="not($tmp4 = '')">
				<xsl:call-template name="convertNewline">
					<xsl:with-param name="in"><xsl:value-of select="$tmp4"/></xsl:with-param>
				</xsl:call-template>
			</xsl:if>
		</xsl:otherwise>
	</xsl:choose>
</xsl:template>

<!-- Image element template -->
<xsl:template name="image">
	<xsl:element name="imageSettings">
		<xsl:if test="image/@name">
			<xsl:attribute name="imageName"><xsl:value-of select="image/@name"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="image/@alignment">
			<xsl:attribute name="alignment">
				<xsl:call-template name="alignmentAttribute">
					<xsl:with-param name="align"><xsl:value-of select="image/@alignment"/></xsl:with-param>
				</xsl:call-template>
			</xsl:attribute>
		</xsl:if>
		<xsl:if test="image/@backcolor">
			<xsl:attribute name="backColor"><xsl:value-of select="image/@backcolor"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="image/@backstyle">
			<xsl:attribute name="backStyle"><xsl:value-of select="image/@backstyle"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="image/@blink">
			<xsl:attribute name="blink"><xsl:value-of select="image/@blink"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="image/@color">
			<xsl:attribute name="color"><xsl:value-of select="image/@color"/></xsl:attribute>
		</xsl:if>
		<xsl:if test="image/@scaled">
			<xsl:attribute name="scaled"><xsl:value-of select="image/@scaled"/></xsl:attribute>
		</xsl:if>
	</xsl:element>
</xsl:template>

<!-- Helper templates -->
<xsl:template name="alignmentAttribute">
	<xsl:param name="align"/>
	<xsl:choose>
		<xsl:when test="$align = 'top-left'">topLeft</xsl:when>
		<xsl:when test="$align = 'top-center'">topCenter</xsl:when>
		<xsl:when test="$align = 'top-right'">topRight</xsl:when>
		<xsl:when test="$align = 'bottom-left'">bottomLeft</xsl:when>
		<xsl:when test="$align = 'bottom-center'">bottomCenter</xsl:when>
		<xsl:when test="$align = 'bottom-right'">bottomRight</xsl:when>
		<xsl:when test="$align = 'mid-left'">middleLeft</xsl:when>
		<xsl:when test="$align = 'mid-center'">middleCenter</xsl:when>
		<xsl:when test="$align = 'mid-right'">middleRight</xsl:when>
		<xsl:otherwise>middleCenter</xsl:otherwise><!-- make defalut as error -->
	</xsl:choose>
</xsl:template>

<xsl:template name="patternAttribute">
	<xsl:param name="pattern"/>
	<xsl:choose>
		<xsl:when test="$pattern = 'none'">none</xsl:when>
		<xsl:when test="$pattern = 'dots'">dots</xsl:when>
		<xsl:when test="$pattern = 'checks'">checks</xsl:when>
		<xsl:when test="$pattern = 'small-box'">smallBoxes</xsl:when>
		<xsl:when test="$pattern = 'med-box'">mediumBoxes</xsl:when>
		<xsl:when test="$pattern = 'large-box'">largeBoxes</xsl:when>
		<xsl:when test="$pattern = 'vert-line'">vLines</xsl:when>
		<xsl:when test="$pattern = 'wide-vert-line'">wideVLines</xsl:when>
		<xsl:when test="$pattern = 'horiz-line'">hLines</xsl:when>
		<xsl:when test="$pattern = 'wide-horiz-line'">wideHLines</xsl:when>
		<xsl:when test="$pattern = 'right-diag'">rDiagonal</xsl:when>
		<xsl:when test="$pattern = 'wide-right-diag'">wideRDiagonal</xsl:when>
		<xsl:when test="$pattern = 'left-diag'">lDiagonal</xsl:when>
		<xsl:when test="$pattern = 'wide-left-diag'">wideLDiagonal</xsl:when>
		<xsl:when test="$pattern = 'hatch'">hatch</xsl:when>
		<xsl:when test="$pattern = 'bricks'">bricks</xsl:when>
		<xsl:when test="$pattern = 'ovals'">ovals</xsl:when>
		<xsl:when test="$pattern = 'diamonds'">diamonds</xsl:when>
		<xsl:when test="$pattern = 'scales'">scales</xsl:when>
		<xsl:when test="$pattern = 'waves'">waves</xsl:when>
		<xsl:otherwise>none</xsl:otherwise><!-- make defalut as error -->
	</xsl:choose>
</xsl:template>

<!-- font attributes template -->
<xsl:template name="fontAttributes">
	<xsl:param name="font"/>

	<xsl:for-each select="/gfx/defs/font">
		<!-- go through each font in the defs node to find a font id that matches the requested font.
			 if found add the font attributes as necessary -->
		<xsl:if test="@id = $font">
			<xsl:if test="@font-family">
				<xsl:attribute name="fontFamily"><xsl:value-of select="@font-family"/></xsl:attribute>
			</xsl:if>
			<xsl:if test="@font-size">
				<xsl:attribute name="fontSize"><xsl:value-of select="@font-size"/></xsl:attribute>
			</xsl:if>

			<xsl:if test="@font-style">
				<xsl:attribute name="italic">
					<xsl:choose>
						<xsl:when test="@font-style = 'italic'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@font-weight">
				<xsl:attribute name="bold">
					<xsl:choose>
						<xsl:when test="@font-weight = 'bold'">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
			<xsl:if test="@text-decoration">
				<xsl:attribute name="underline">
					<xsl:choose>
						<xsl:when test="contains(@text-decoration, 'underline')">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
				<xsl:attribute name="strikethrough">
					<xsl:choose>
						<xsl:when test="contains(@text-decoration, 'line-through')">true</xsl:when>
						<xsl:otherwise>false</xsl:otherwise>
					</xsl:choose>
				</xsl:attribute>
			</xsl:if>
		</xsl:if>
	</xsl:for-each>
</xsl:template>

<!-- Animation elements template -->
<xsl:template name="animations">
	<!-- if we have the visibility animation then add it -->
	<xsl:if test="animations/animation/@type = 'visibility'">
		<animations>
			<animateVisibility>
				<xsl:if test="animations/animation/@expr">
					<xsl:attribute name="expression"><xsl:value-of select="animations/animation/@expr"/></xsl:attribute>
				</xsl:if>
				<xsl:if test="animations/animation/@visible">
					<xsl:attribute name="expressionTrueState">
						<xsl:choose>
							<xsl:when test="animations/animation/@visible = 'true'">visible</xsl:when>
							<xsl:otherwise>invisible</xsl:otherwise>
						</xsl:choose>
					</xsl:attribute>
				</xsl:if>
			</animateVisibility>
		</animations>
	</xsl:if>
</xsl:template>

<!-- Connection elements template -->
<xsl:template match="connections" name="connections">
	<!-- if we have the indicator connection and it has an expression, then add it -->
	<xsl:if test="connections/connection/@name = 'indicator'">
		<xsl:if test="connections/connection/@expr">
			<connections>
				<connection name="Indicator">
					<xsl:attribute name="expression"><xsl:value-of select="connections/connection/@expr"/></xsl:attribute>
				</connection>
			</connections>
		</xsl:if>
	</xsl:if>
</xsl:template>

</xsl:stylesheet>
